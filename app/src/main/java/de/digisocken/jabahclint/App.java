package de.digisocken.jabahclint;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jxmpp.jid.EntityBareJid;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class App extends Application {
    private static Context contextOfApplication;
    private static AbstractXMPPConnection mConnection;

    public static final String TAG = MainActivity.class.getSimpleName();
    public static SharedPreferences pref;

    public static final int DEFAULT_NIGHT_START = 18;
    public static final int DEFAULT_NIGHT_STOP = 6;

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        contextOfApplication = getApplicationContext();
        pref = PreferenceManager.getDefaultSharedPreferences(
                App.getContextOfApplication()
        );

        if (!pref.contains("nightmode_use_start")) {
            pref.edit().putInt("nightmode_use_start", DEFAULT_NIGHT_START).commit();
        }
        if (!pref.contains("nightmode_use_stop")) {
            pref.edit().putInt("nightmode_use_stop", DEFAULT_NIGHT_STOP).commit();
        }

        if (mConnection == null) {
            try {
                mConnection = new XMPPTCPConnection(
                        pref.getString("XMPP_LOGIN", ""),
                        pref.getString("XMPP_PASS", "")
                );
                if (!mConnection.isConnected()) {
                    mConnection.connect().login();
                    ChatManager.getInstanceFor(App.getConnection()).addIncomingListener(new IncomingChatMessageListener() {
                        @Override
                        public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {
                            ContentValues values = new ContentValues();
                            Date date = new Date();
                            values.put(EntryContract.DbEntry.COLUMN_Date, EntryContract.dbFriendlyDate(date));
                            values.put(EntryContract.DbEntry.COLUMN_User, from.asBareJid().toString());
                            values.put(EntryContract.DbEntry.COLUMN_Body, message.getBody());
                            getContentResolver().insert(EntryContentProvider.CONTENT_URI, values);
                            notificate(from.asBareJid().toString(), message.getBody(), date);
                        }
                    });
                }
            } catch (SmackException e) {
                e.printStackTrace();
                mConnection = null;
            } catch (IOException e) {
                e.printStackTrace();
                mConnection = null;
            } catch (XMPPException e) {
                e.printStackTrace();
                mConnection = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
                mConnection = null;
            }
        }
    }

    public static AbstractXMPPConnection getConnection() {
        return mConnection;
    }

    public static Context getContextOfApplication() {
        return contextOfApplication;
    }

    public static void notificate(String user, String body, Date date) {
        Context ctx = App.getContextOfApplication();
        Intent notificationIntent = new Intent(ctx, MainActivity.class);
        notificationIntent.setFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP
        );
        PendingIntent pi = PendingIntent.getActivity(ctx, 0, notificationIntent, 0);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager nManager =
                (NotificationManager) ctx.getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder ncomp = new NotificationCompat.Builder(ctx);
        ncomp.setContentTitle(user);
        ncomp.setContentText(body);
        ncomp.setTicker(body);
        ncomp.setContentIntent(pi);
        ncomp.setSound(sound);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            Bitmap largeIcon = getBitmapFromVectorDrawable(R.drawable.ic_logo);
            NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
            bigStyle.bigText(body);
            ncomp.setLargeIcon(largeIcon);
            ncomp.setStyle(bigStyle);
            ncomp.setLights(Color.parseColor("#00ff00ff"), 500, 200);
            ncomp.setSmallIcon(R.drawable.ic_logo_sw);
        } else {
            ncomp.setSmallIcon(R.drawable.ic_logo);
        }
        ncomp.setAutoCancel(true);
        Notification noti = ncomp.build();
        noti.flags |= Notification.FLAG_AUTO_CANCEL;
        nManager.notify((int) date.getTime(), noti);
    }


    public static Bitmap getBitmapFromVectorDrawable(int drawableId) {
        Context context = App.getContextOfApplication();
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static boolean inTimeSpan(int startH, int stopH) {
        int nowH = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        if (startH == stopH && startH == nowH) return true;
        if (startH > stopH && (nowH <= stopH || nowH >= startH)) return true;
        if (startH < stopH && nowH >= startH && nowH <= stopH) return true;
        return false;
    }
}
