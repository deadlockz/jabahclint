package de.digisocken.jabahclint;

import android.provider.BaseColumns;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EntryContract {

    public static final String AUTHORITY = "de.digisocken.jabahclint.contentprovider";
    public static final String DEFAULT_SORTORDER = DbEntry.COLUMN_Date +" DESC";

    public static class DbEntry implements BaseColumns {
        public static final String TABLE_NAME = "entries";

        public static final String COLUMN_User = "e_user";
        public static final String COLUMN_Body = "e_body";
        public static final String COLUMN_Date = "e_date";
    }

    // Useful SQL query parts
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String DATE_TYPE = " DATETIME";
    private static final String COMMA_SEP = ",";
    public static final String DATABASE_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    // Useful SQL queries
    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DbEntry.TABLE_NAME + " (" +
                    DbEntry._ID + INTEGER_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    DbEntry.COLUMN_User + TEXT_TYPE + COMMA_SEP +
                    DbEntry.COLUMN_Body + TEXT_TYPE + COMMA_SEP +
                    DbEntry.COLUMN_Date + DATE_TYPE + " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DbEntry.TABLE_NAME;

    public static final String[] projection = {
            DbEntry._ID,
            DbEntry.COLUMN_User,
            DbEntry.COLUMN_Body,
            DbEntry.COLUMN_Date
    };

    public static String dbFriendlyDate(Date date) {
        SimpleDateFormat formatOut = new SimpleDateFormat(DATABASE_DATETIME_FORMAT, Locale.ENGLISH);
        return formatOut.format(date);
    }
}
