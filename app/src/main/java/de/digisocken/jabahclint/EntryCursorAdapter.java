package de.digisocken.jabahclint;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EntryCursorAdapter extends CursorAdapter {
    Activity activity;
    String me;

    public EntryCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        activity = (Activity) context;
        me = App.pref.getString("XMPP_LOGIN", "xxx");
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.msg_line, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tu = (TextView) view.findViewById(R.id.line_user);
        TextView tb = (TextView) view.findViewById(R.id.line_body);
        TextView td = (TextView) view.findViewById(R.id.line_date);
        String user = cursor.getString(cursor.getColumnIndexOrThrow(EntryContract.DbEntry.COLUMN_User));
        String body = cursor.getString(cursor.getColumnIndexOrThrow(EntryContract.DbEntry.COLUMN_Body));
        if (user.equals(me)) {
            tu.setPadding(0,0,0,0);
            tb.setPadding(0,0,0,0);
            user = activity.getString(R.string.me) + " (" + user + ")";
        }

        Date date = null;
        SimpleDateFormat formatIn = new SimpleDateFormat(
                EntryContract.DATABASE_DATETIME_FORMAT, Locale.ENGLISH
        );
        try {
            date = formatIn.parse(cursor.getString(
                    cursor.getColumnIndexOrThrow(EntryContract.DbEntry.COLUMN_Date)
            ));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tu.setText(user);
        tb.setText(body);


        td.setText(new SimpleDateFormat("d. MMM HH:mm").format(date));
        if (cursor.getPosition()%2==0) {
            view.setBackgroundColor(ContextCompat.getColor(
                    activity.getApplicationContext(),
                    R.color.evenCol
            ));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(
                    activity.getApplicationContext(),
                    R.color.oddCol
            ));
        }
    }
}
