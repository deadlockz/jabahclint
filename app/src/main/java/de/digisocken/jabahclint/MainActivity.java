package de.digisocken.jabahclint;

import android.app.LoaderManager;
import android.app.UiModeManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.Date;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private EntryCursorAdapter entryCursorAdapter;
    private ListView entryList;
    private TextView tv;
    private UiModeManager umm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        umm = (UiModeManager) getSystemService(Context.UI_MODE_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        tv = (TextView) findViewById(R.id.msgEdit);

        getLoaderManager().initLoader(0, null, this);
        entryList = (ListView) findViewById(R.id.msgList);
        entryCursorAdapter = new EntryCursorAdapter(this, null, 0);
        entryList.setAdapter(entryCursorAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMsg(tv.getText().toString());
            }
        });


        FloatingActionButton fabSettings = (FloatingActionButton) findViewById(R.id.fabSettings);
        fabSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent prefInt = new Intent(MainActivity.this, PreferencesActivity.class);
                startActivity(prefInt);
            }
        });

        entryCursorAdapter.notifyDataSetChanged();

    }

    void sendMsg(String msg) {
        if (App.getConnection().isConnected()) {
            try {
                EntityBareJid recipientJid = JidCreate.entityBareFrom(
                        App.pref.getString("XMPP_OTHER", "")
                );
                ChatManager.getInstanceFor(App.getConnection()).chatWith(recipientJid).send(msg);
                ContentValues values = new ContentValues();
                Date date = new Date();
                values.put(EntryContract.DbEntry.COLUMN_Date, EntryContract.dbFriendlyDate(date));
                values.put(
                        EntryContract.DbEntry.COLUMN_User,
                        App.pref.getString("XMPP_LOGIN", getString(R.string.me))
                );
                values.put(EntryContract.DbEntry.COLUMN_Body, msg);
                getContentResolver().insert(EntryContentProvider.CONTENT_URI, values);
            } catch (XmppStringprepException e) {
                e.printStackTrace();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tv.setText("");
            doSneak(getString(R.string.sended));
        } else {
            doSneak(getString(R.string.not_connected));
            entryCursorAdapter.notifyDataSetChanged();
        }
    }

    void doSneak(String hint) {
        Toast.makeText(App.getContextOfApplication(), hint, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        int startH = App.pref.getInt("nightmode_use_start", App.DEFAULT_NIGHT_START);
        int stopH = App.pref.getInt("nightmode_use_stop", App.DEFAULT_NIGHT_STOP);
        if (App.inTimeSpan(startH, stopH) && umm.getNightMode() != UiModeManager.MODE_NIGHT_YES) {
            umm.setNightMode(UiModeManager.MODE_NIGHT_YES);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        if (!App.inTimeSpan(startH, stopH) && umm.getNightMode() != UiModeManager.MODE_NIGHT_NO) {
            umm.setNightMode(UiModeManager.MODE_NIGHT_NO);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        super.onResume();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(
                this,
                EntryContentProvider.CONTENT_URI,
                EntryContract.projection,
                null,
                null,
                EntryContract.DEFAULT_SORTORDER
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (entryCursorAdapter != null) {
            entryCursorAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (entryCursorAdapter != null) {
            entryCursorAdapter.swapCursor(null);
        }
    }
}
